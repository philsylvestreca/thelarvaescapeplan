

import javax.swing.*;
import java.awt.*;

public class MainClass {

	
	public static void main(String[] args) {
		
		//user inputs PvP or PvAI and if PvAI, what faction the human player will control
		Object[] options = {"Player vs. Player", "Player vs. AI"};
		Object[] options1 = {"Larva", "Birds"};
		JFrame f = new JFrame(); //dialog box
		JFrame e = new JFrame();
		int n = JOptionPane.showOptionDialog(f, "What mode do you want to play?","Choose a mode",JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,null,options, options[0]);
		
		
		 //Create board & text area
	
		Board b = new Board();
		JPanel textPanel = new JPanel(new BorderLayout()); 
		JTextArea text = new JTextArea(5,20); 
		text.setMinimumSize(new Dimension(250, 350));
		text.setEditable(false);
		
		
		if(n == 0){ //case PvP
			
			JLabel turnLbl = new JLabel("P1's Turn");
			//create a new view and a new hbgame
			HungryBirdsGame g = new HungryBirdsGame(n, turnLbl); //initialize a HungryBirdsGame of the selected mode
			Display view = new Display(b, text,g);
			
			JScrollPane scroller = new JScrollPane(text);
			scroller.setMinimumSize(new Dimension(250,250));
			
			textPanel.add(scroller, BorderLayout.CENTER);
			textPanel.add(turnLbl, BorderLayout.NORTH);
			
			JFrame frame = new JFrame();
			frame.setLayout(new BorderLayout());
			frame.add(view, BorderLayout.CENTER);
			frame.add(textPanel, BorderLayout.SOUTH);
			frame.pack();
			frame.setMinimumSize(new Dimension(530,640));  
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setResizable(false); 
			frame.setVisible(true);
		}else{ //case PvAI
			int m = JOptionPane.showOptionDialog(e, "What faction do you want to control?", "Choose Larva or Birds", JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,null,options1, options1[0]);
			//if m=0 human is Larva
			//if m=1 human is Birds
			
			JLabel turnLbl = new JLabel("It's Your Turn!!"); //initializes turn lbl
			HungryBirdsGame g = new HungryBirdsGame(n, m, turnLbl);
			Display view = new Display(b, text,g);

			JScrollPane scroller = new JScrollPane(text);
			scroller.setMinimumSize(new Dimension(250,250));
			
			textPanel.add(turnLbl, BorderLayout.NORTH);
			textPanel.add(scroller, BorderLayout.CENTER);
			
			
			JFrame frame = new JFrame();
			frame.setLayout(new BorderLayout());
			frame.add(view, BorderLayout.CENTER);
			frame.add(textPanel, BorderLayout.SOUTH);
			frame.pack();
			frame.setMinimumSize(new Dimension(530,640));  
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setResizable(false); 
			frame.setVisible(true);
			
		}

	}

}
