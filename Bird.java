
import java.awt.*;
import javax.imageio.*;

import java.awt.image.BufferedImage;
import java.io.*;

public class Bird extends Faction {
	private BufferedImage img;
	
	
	public Bird(int x, int y, boolean kind){
		super(x,y, kind);
		img = null;
		
			try {
			    img = ImageIO.read(new File("angry-Bird-icon.png"));
			} catch (IOException e) {
				System.out.println("can't find pic");
			}
		
	}
	
	public void draw(Graphics g){
		g.drawImage(img, super.getPosX(), super.getPosY(), null);
	}
	
	
	public boolean moveIsLegal(Point p, Board b){
		int originX = (int)super.getCurrSquare().getX();
		int originY = (int)super.getCurrSquare().getY();
		int destX = (int)p.getX()/62;
		int destY = (int)p.getY()/62;
		
		/*
		 * checks if the square being moved to has a faction
		 */
		if(b.hasaPiece(destX, destY)){
			
				return false;
			
		}
		
		
		if(Math.abs(destX - originX) == Math.abs(destY - originY) && (destX >= 0 && destX <= 7) && (destY >= 0 && destY <= 7)){ //case that it moved in the correct pattern
			
			if(destY != originY -1){	//check moves forward
				return false;
			}
			
			if(Math.abs(destX - originX) > 1 && Math.abs(destY - originY) >1){	//checks moves only by 1
				return false;
			}
			
			//if(destY == 0){
			//	return false;
			//}
			
			if(destX - originX > 0 && destY - originY > 0){ //checks diag
				for(int i= 1; i< destX-originX; i++){   
					if(b.hasaPiece(originX + i,originY + i)){
						return false;
					}
				}
				return true;
			}
			if(destX - originX < 0 && destY - originY < 0){ //checks diag
				for(int i = 1; i< originX - destX; i++){
					if(b.hasaPiece(destX + i, destY + i)){
						return false;
					}
				}
				return true;
			}
			if(((destX - originX) > 0) && ((destY - originY) < 0)){ //checks diag
				for(int i = 1; i< destX - originX; i++){
					if(b.hasaPiece(originX + i, originY - i)){
						return false;
					}
				}
				System.out.println("Legal move: " + getType()  + " from: (" + originX + "," + originY + ") to (" + destX + "," + destY + ")");

				return true;
			}
			if(destX - originX < 0 && destY - originY > 0){ //checks diag
				for(int i = 1; i< Math.abs(destX - originX); i++){
					if(b.hasaPiece(originX -i, originY + i)){
						return false;
					}
				}
				return true;
			}
			return true;
		}
		return false; //return false if not valid
	}
	
		public String getType(){
		return "Bird";
	}
}
