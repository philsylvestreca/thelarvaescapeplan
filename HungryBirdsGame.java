
import javax.swing.*;

public class HungryBirdsGame {
	private boolean turn; //false means larva turn, true means bird turn
	private int mode; //0 => player vs. player, 1 =>  minimax
	private int selector; //if 0 => human controls larva, if 1 => human controls bird
	private boolean victory; //false => no victory, true => game over
	private AI player;
	private JLabel label; 
	
	public HungryBirdsGame(int mode, int selector, JLabel label){
		turn = false;
		this.mode = mode;
		this.selector = selector;
		victory = false;
		this.label = label;
		player = new AIAlgorithm();
		
	}
	public HungryBirdsGame(int mode, JLabel label){
		turn = false;
		this.mode = mode;
		victory = false;
		
		
		this.label = label;
	}
	
	
	public AI getAI(){
		return player;
	}
	
	
	public boolean getVictory(){
		return victory;
	}
	
	
	public int getSelector(){
		return selector;
	}
	
	public int getMode(){
		return mode;
	}
	
	
	
	public boolean getTurn(){
		return turn;
	}
	
	
	public void chgTurn(){
		turn = !turn;
		if(mode == 0){
			if(!turn){
				label.setText("P1's Turn");
			}else{
				label.setText("P2's Turn");
			}
		}else if(mode == 1){
			if(!turn){
				label.setText("It's Your turn!!");
			}else{
				label.setText("AI's turn!");
			}
		}
	}
	
	
	public void chkVictory(Board b){
		victory = false; //victory defaults to false if no Larva crossed the fence
		if(turn == false){ //case that it was bird turn
			/*
			 * Iterate through the board
			 */
			for(int i = 0; i<8; i++){
				
					if(b.hasaPiece(i, 7)){
						if(b.getPos(i,7).getType().equalsIgnoreCase("Larva")){ 
							if(b.getPos(i, 7).getKind() == false){ 				//checks if a Larva crossed the fence
								victory =  true;
							}
						}
					}
			}
			
		}
		
		
		else{ //case that it was larva turn
			for(int i =0; i<8; i++){
				for(int j=0; j<8; j++){
					if(b.hasaPiece(i, j)){
						if(b.getPos(i,j).getType().equalsIgnoreCase("Larva")){
							try{
							if(b.hasaPiece(i+1,j+1) && b.hasaPiece(i-1,j+1) && b.hasaPiece(i+1,j-1) && b.hasaPiece(i-1,j-1)){
								victory = true;
							}
							}
							catch(ArrayIndexOutOfBoundsException e){}
							
							if(i==0 && j==0){				//top left corner
								if(b.hasaPiece(i+1,j+1)){
									victory = true;
								}
							}
							
							if(i==0 && j==7){				//top right corner
								if(b.hasaPiece(i-1,j+1)){
									victory = true;
								}
							}
							
							if(j==0 && i!=0 && i!=7){		//top row, not corner
								if(b.hasaPiece(i+1,j+1) && b.hasaPiece(i-1,j+1)){
									victory = true;
								}
							}
							
							if(i==0 && j!=0 && j!=7){		//leftmost column, not corner
								if(b.hasaPiece(i+1,j+1) && b.hasaPiece(i+1,j-1)){
									victory = true;
								}
							}
							
							if(i==7 && j!=0 && j!=7){		//rightmost column, not corner
								if(b.hasaPiece(i-1,j+1) && b.hasaPiece(i-1,j-1)){
									victory = true;
								}
							}
							
						}
					}
				}
			}
		}
	}
	
}

