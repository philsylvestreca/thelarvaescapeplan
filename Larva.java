
import java.awt.*;
import javax.imageio.*;

import java.awt.image.BufferedImage;
import java.io.*;


public class Larva extends Faction {
	private BufferedImage img;
	
	
	public Larva(int x, int y, boolean kind){
		super(x,y, kind);
		img = null;
		
			try {
			    img = ImageIO.read(new File("Green_Larva.png"));
			} catch (IOException e) {
				System.out.println("can't find pic");
			}
		
	
	}
	
	
	public void draw(Graphics g){
		g.drawImage(img, super.getPosX(), super.getPosY(), null);
	}
	
	
	public boolean moveIsLegal(Point p, Board b){
		int originX = (int)super.getCurrSquare().getX();
		int originY = (int)super.getCurrSquare().getY();
		int destX = (int)p.getX()/62;
		int destY = (int)p.getY()/62;
		/*
		 * checks if the square being moved to has a faction
		 */
		if(b.hasaPiece(destX, destY)){
			
				return false;
			
		}
		
		if(Math.abs(destX - originX) == Math.abs(destY - originY) && (destX >= 0 && destX <= 7) && (destY >= 0 && destY <= 7)){ //case that it moved in the correct pattern
			
			
			if(Math.abs(destX - originX) > 1 && Math.abs(destY - originY) >1){	//checks that it moves only 1 position
				return false;
			}
			
			if(destX - originX > 0 && destY - originY > 0){ //checks if moved to the right and down, iterates through board,
				for(int i= 1; i< destX-originX; i++){   // finds if there are any pieces in the way
					if(b.hasaPiece(originX + i,originY + i)){
						return false;
					}
				}
				return true;
			}
			if(destX - originX < 0 && destY - originY < 0){ //checks if it moved left and up
				for(int i = 1; i< originX - destX; i++){
					if(b.hasaPiece(destX + i, destY + i)){
						return false;
					}
				}
				return true;
			}
			if(((destX - originX) > 0) && ((destY - originY) < 0)){ //checked if it moved right and up
				for(int i = 1; i< destX - originX; i++){
					if(b.hasaPiece(originX + i, originY - i)){
						return false;
					}
				}
				System.out.println("Legal move: " + getType()  + " from: (" + originX + "," + originY + ") to (" + destX + "," + destY + ")");

				return true;
			}
			if(destX - originX < 0 && destY - originY > 0){ //checks if it moved left and down
				for(int i = 1; i< Math.abs(destX - originX); i++){
					if(b.hasaPiece(originX -i, originY + i)){
						return false;
					}
				}
				return true;
			}
			return true;
		}
		return false; //return false if it went in wrong pattern
	}
	
	public String getType(){
		return "Larva";
	}

}
