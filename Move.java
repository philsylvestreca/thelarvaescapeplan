
public class Move {
	private int originX;  //originX and originY keep track of where the the faction is coming from
	private int originY;
	private int DestX;  //DestX and DestY keep track of where the faction is going
	private int DestY;
	private Faction p; //p is a copy of the faction that is to be moved
	

	public Move(int originX, int originY, int DestX, int DestY, Faction faction){
		this.originX = originX;
		this.originY = originY;
		this.DestX = DestX;
		this.DestY = DestY;
		
		//makes a copy of the faction and assigns it to p
		/*
		 * Note that a copy is used to prevent faction positions from being moved around
		 * when the AI is making hypothetical moves
		 */
		if(faction.getKind() == false){ //case that faction is larva
			if(faction.getType() == "Larva"){
				p = new Larva(faction.getPosX()/62,faction.getPosY()/62,false);
			}
		}else if(faction.getKind() == true){ //case that faction is bird
			if(faction.getType()== "Bird"){
				p = new Bird(faction.getPosX()/62,faction.getPosY()/62,true);
			}
		}
	}
	
	
	public int getOriginX(){
		return originX;
	}
	
	
	public int getOriginY(){
		return originY;
	}
	
	
	public int getDestX(){
		return DestX;
	}
	
	
	public int getDestY(){
		return DestY;
	}
	
	
	public Faction getFaction(){
		return p;
	}
	
	
	public String toString(){
		String text = p.getType() + " moved from (" + originX + "," + originY + ") to (" + DestX + "," + DestY + ")";
		return text;
	}
}
