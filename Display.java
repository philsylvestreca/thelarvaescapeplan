


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Display extends JComponent{
	
	private final String[] columns = {"A", "B", "C", "D", "E", "F", "G", "H"}; 
	private final Board board; //instance of board class
	private HungryBirdsGame hbgame; //instance of HungryBirdsGame class
	private Point mousePt; //tracks mouse loc
	private JTextArea textBox;
	private String text; 
	
	
	public Display(Board board, JTextArea textBox, HungryBirdsGame hbgame){
		this.board = board;
		this.hbgame = hbgame;
		this.textBox  = textBox;
		text = "";
		
		 // MouseListener manages mouse press events.  
		
		
		
		 //MouseListener for PvP mode
		 
		if(hbgame.getMode() == 0){
			addMouseListener(new
					MouseAdapter(){
						public void mousePressed(MouseEvent evnt){
							mousePt = evnt.getPoint(); //sets mouse point to the curr pt
							//iterates through swuares on board
							if(!Display.this.hbgame.getVictory()){
								for(int i = 0; i<8; i++){ 
									for(int j = 0; j<8; j++){
										if(Display.this.board.hasaPiece(i, j)){ 
											if(Display.this.board.getPos(i, j).contains(evnt.getPoint())){ 
												if(Display.this.board.getPos(i,j).getKind() == Display.this.hbgame.getTurn()){ //check turn
													Display.this.board.getPos(i,j).setSelected(true); //set selected to true
													Display.this.text += Display.this.board.getPos(i,j).getType() + " selected! \n"; 
													Display.this.textBox.setText(Display.this.text);
												}
											}
										}
									}
								}
							}
						}
			});
		}
		
		//MouseListener for PvAI mode
		
		if(hbgame.getMode() != 0){
			addMouseListener(new
					MouseAdapter(){
						public void mousePressed(MouseEvent evnt){
							mousePt = evnt.getPoint(); //sets mouse point to the curr pt
							//iterates through swuares on board
							if(!Display.this.hbgame.getVictory()){
								for(int i = 0; i<8; i++){ 
									for(int j = 0; j<8; j++){
										if(Display.this.board.hasaPiece(i, j)){ 
											if(Display.this.board.getPos(i, j).contains(evnt.getPoint())){ 
												if(Display.this.board.getPos(i,j).getKind() == Display.this.hbgame.getTurn()){ //check turn
													Display.this.board.getPos(i,j).setSelected(true); //set selected to true
													Display.this.text += Display.this.board.getPos(i,j).getType() + " selected! \n";
													Display.this.textBox.setText(Display.this.text);
												}
											}
										}
									}
								}
							}
						}
			});
			if(Display.this.hbgame.getMode() != 0){ //case PvAI
				if(Display.this.hbgame.getSelector() == 1){
				//thread runs in bkground, compute best move for AI and performs it
				Thread t = new Thread(new Runnable(){
					public void run(){
						Display.this.text += Display.this.hbgame.getAI().mkMove(Display.this.board, Display.this.hbgame.getSelector()); 
						Display.this.textBox.setText(Display.this.text);
						Display.this.hbgame.chkVictory(Display.this.board);
						if(Display.this.hbgame.getVictory()){
							if(Display.this.hbgame.getTurn() == false){
								Display.this.text += "Larva has crossed the fence and won the hbgame!";
							}
							else{
								Display.this.text += "Bird has cornered the Larva and won the hbgame!";
							}
							Display.this.textBox.setText(Display.this.text);
						}else{
						Display.this.hbgame.chgTurn();
						}
						repaint();  //refresh board everytime the AI makes a move
					}
				});
				
				t.start(); //begins thread

			}
			}
		}
		
		//MotionListener listens for and manages when mouse is dragged
		 
		addMouseMotionListener(new MouseMotionAdapter(){
			public void mouseDragged(MouseEvent evnt){
				Point oldMousePt = mousePt; 
				mousePt = evnt.getPoint();
				for(int i = 0; i<8; i++){ 
					for(int j=0; j<8; j++){
						if(Display.this.board.hasaPiece(i, j)){
							if(Display.this.board.getPos(i,j).isSelected()){
								double dx = mousePt.getX() - oldMousePt.getX(); 
								double dy = mousePt.getY() - oldMousePt.getY();
								Display.this.board.getPos(i, j).translate((int)dx,(int)dy); //translate faction
							}
						}
					}
				}
				repaint(); //refresh Display everytime mouse is dragged
			}
		});
		
		//actionListener listens for when mouse is released
		
		addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e){
				boolean factionSelected = false; //faction selected?
				Faction selectedFaction = null; //what faction is selected?
				
				int sourceX = 0; 
				int sourceY = 0;
				
				
				for(int i=0; i<8; i++){
					for(int j=0; j<8; j++){
						if(Display.this.board.hasaPiece(i, j)){ 
							if(Display.this.board.getPos(i, j).isSelected()){ //check if faction was selected
								factionSelected = true;
								selectedFaction = Display.this.board.getPos(i,j); 
								sourceX = i;
								sourceY = j;
							}
						}
					}
				}
				if(factionSelected){
					if(selectedFaction.moveIsLegal(e.getPoint(), Display.this.board)){ //chk legal moves
						int DestX = ((int)e.getPoint().getX())/62; 
						int DestY = ((int)e.getPoint().getY())/62;
						
						
						Display.this.board.clrPos(sourceX, sourceY);
						Display.this.board.setPos(DestX, DestY, selectedFaction);
						//snap to square
						if(selectedFaction.setLocation(DestX, DestY)) System.out.println("loc set");
						
						Display.this.text += selectedFaction.getType() + " was moved to: " + Display.this.columns[DestX] +  (DestY+1) + "\n"; 
						System.out.println("square");
						
						
						Display.this.textBox.setText(Display.this.text); 
						
						Display.this.hbgame.chkVictory(Display.this.board);
						if(Display.this.hbgame.getVictory()){
							if(Display.this.hbgame.getTurn() == false){
								Display.this.text += "Larva has crossed the fence and won the hbgame!";
							}
							else{
								Display.this.text += "Bird has cornered the Larva and won the hbgame!";
							}
							Display.this.textBox.setText(Display.this.text);
						}else{
							Display.this.hbgame.chgTurn();
							if(Display.this.hbgame.getMode() != 0 /*&& Display.this.hbgame.getSelector() == 0*/){ //case that the hbgame is playing with AI and human controls larva
								
								//thread runs in bkground, compute best move for AI and performs it

								
								Thread t = new Thread(new Runnable(){
									public void run(){
										Display.this.text += Display.this.hbgame.getAI().mkMove(Display.this.board, Display.this.hbgame.getSelector()); 
										Display.this.textBox.setText(Display.this.text);
										Display.this.hbgame.chkVictory(Display.this.board);
										if(Display.this.hbgame.getVictory()){
											if(Display.this.hbgame.getTurn() == false){
												Display.this.text += "Larva has crossed the fence and won the hbgame!";
											}
											else{
												Display.this.text += "Bird has cornered the Larva and won the hbgame!";
											}
											Display.this.textBox.setText(Display.this.text);
										}else{
										Display.this.hbgame.chgTurn();
										}
										repaint();  //refresh board everytime AI makes move
									}
								});
								
								t.start(); //begins thread

							}
						}
					}else{ //illegal move
						//backtrack to original square
						selectedFaction.setLocation(sourceX, sourceY);
						Display.this.text += "This is an illegal move! \n";
						Display.this.textBox.setText(Display.this.text);
					}
					selectedFaction.setSelected(false);
				}
				repaint();
			}
		});
	}
	
	@Override
	public void paintComponent(Graphics g){
		Graphics2D g2 = (Graphics2D)g;
		boolean isBlack = false;
		
		//draws checkered 8x8 grid
		 
		for(int i = 0; i<8; i++){
			isBlack = !(isBlack);
			for(int j = 0; j<8; j++){
				isBlack = !(isBlack);
				Rectangle rectangle = new Rectangle(i*62,j*62,62,62);
				if(isBlack){	
					g2.setColor(Color.darkGray);
				}else{
					g2.setColor(Color.white);
				}
				g2.fill(rectangle);
			}
		}
		
		//draws the faction for all squares with a faction.  
		
		for(int i = 0; i<8; i++){
			for(int j = 0; j<8; j++){
				if(board.hasaPiece(i, j)){ 
					board.getPos(i, j).draw(g2);
				}
			}
		}
		//draws pieces
		for(int i=0; i<8; i++){
			for(int j =0; j<8; j++){
				if(board.hasaPiece(i, j)){
					if(board.getPos(i, j).isSelected()){
						board.getPos(i,j).draw(g2);
					}
				}
			}
		}
	}
}
