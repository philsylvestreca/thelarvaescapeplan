

import java.awt.*;
import java.awt.geom.*;

public abstract class Faction {
	
	private int x; //locations denoted are top-left hand corner of squares.
	private int y;
	private Point currSquare; //keeps track of what square the faction is on, in terms of squares on the board
	private boolean kind; //true:bird, false:larva
	
	private boolean selected; //keeps track of if  a faction has been selected or not
	
	
	public Faction(int x, int y, boolean kind ){ //note that false is larva and true is bird for the kind
		currSquare = new Point(x,y);
		this.x = x*62;
		this.y = y*62;
		this.selected = false;
		this.kind = kind;
	}
	
	
	public Point getCurrSquare(){
		return currSquare;
	}
	
	
	public void setSelected(boolean selected){
		this.selected =  selected;
	}
	
	
	public boolean isSelected(){
		return selected;
	}
	
	
	public Point getLocation(){
		return new Point(x,y);
	}
	
	public int getPosX(){
		return x;
	}
	
	
	public int getPosY(){
		return y;
	}
	
	
	public boolean contains(Point2D p){
		return x<= p.getX() && (x + 62) >= p.getX() && y<= p.getY() && (y+62) >= p.getY();
	}
	
	
	public void translate(int dx, int dy){
		x += dx;
		y += dy;
	}
	
	
	public boolean setLocation(int row, int col){
		x = row*62;
		y  = col*62;
		currSquare.setLocation(row, col);
		return true; //for debug
	}
	
	public boolean getKind(){
		return kind;
	}
	
	public abstract String getType();
	
	public abstract void draw(Graphics g);
	
	public abstract boolean moveIsLegal(Point p, Board b);
}
