

//import java.util.*;

public class Board {
	private Faction[][] board;
	
	
	
	public Board(){
		
		board = new Faction[8][8]; //initializes board
		
		for(int i = 0; i<8; i++){
			for(int j = 0; j<8; j++){
				board[i][j] = null;
			}
		}
		//places the pieces on the board 
		
		setPos(3,6,new Larva(3,6,false));
		
		setPos(0,7,new Bird(0,7,true));
		setPos(2,7,new Bird(2,7,true));
		setPos(4,7,new Bird(4,7,true));
		setPos(6,7,new Bird(6,7,true));
		
		
		
	}
	
	public Board(Board b){
		board = new Faction[8][8]; //initializes board
		
		for(int i = 0; i<8; i++){
			for(int j = 0; j<8; j++){
				board[i][j] = null;
			}
		}
		
		
		for(int i = 0; i<8; i++){
			for(int j=0; j<8; j++){
				if(b.hasaPiece(i,j)){
					if(b.getPos(i, j).getKind() == false){ //case that faction is larva
						if(b.getPos(i,j).getType() == "Larva"){
							board[i][j] = new Larva(i,j,false);
						}else if(b.getPos(i,j).getType() == "Bird"){
							board[i][j] = new Bird(i,j,false);
						}
					}else{ //case that faction is bird
						if(b.getPos(i,j).getType() == "Larva"){
							board[i][j] = new Larva(i,j,true);
						}else if(b.getPos(i,j).getType() == "Bird"){
							board[i][j] = new Bird(i,j,true);
						}
					}
				}
			}
		}
	}

	
	public Faction getPos(int row, int col){
		return board[row][col];
	}
	
	
	public void setPos(int row, int col, Faction faction){
		board[row][col] = faction;
	}
	
	public void clrPos(int row, int col){
		board[row][col] = null;
	}
	
	
	public boolean hasaPiece(int row, int col){
		if(getPos(row,col) != null){
			return true;
		}else{
			return false;
		}
	}
	
}
