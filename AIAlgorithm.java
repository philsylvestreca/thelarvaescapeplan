

import java.util.*;
import java.awt.*;
	
public class AIAlgorithm implements AI {
	private static final int DEPTH = 3;
	private int numTurns;
	private boolean sPlayer;
	
	
	@Override
	public String mkMove(Board b, int Selector) {
		int selector = Selector;
		if(selector == 0) sPlayer = true;
		else sPlayer = false;
		// TODO Auto-generated method stub
		//generate all legal moves
		Move bestMove; //
		int bestMoveScore; //score of that best move
		
		ArrayList<Board> Boards = new ArrayList<Board>(); //possible boards
		ArrayList<Move> moves  = new ArrayList<Move>(); //possible moves 
		
		/*
		 * iterate, gen all possible moves, push them back in moves
		 */
		
		for(int i = 0; i<8; i++){
			for(int j=0; j<8; j++){
				if(b.hasaPiece(i,j)){
					Faction faction = b.getPos(i,j);
					
					if(faction.getKind() == sPlayer){
						for(int k=0; k<8; k++){
							for(int l=0; l<8; l++){
								if(faction.moveIsLegal(new Point(k*62,l*62),b)){ 
									Move move = new Move(i,j,k,l,faction);
									moves.add(move);
									Board newBoard = new Board(b); //calls the copy constr of board class
									trigMove(newBoard, move); //performs move on new board
									Boards.add(newBoard);
								}
							}
						}
					}
										
				}
			}
		}
		//ini bestMove
		bestMove = moves.get(0);
		bestMoveScore = evalPos(Boards.get(0), Integer.MIN_VALUE, Integer.MAX_VALUE, DEPTH, !sPlayer);
		
		
		if(numTurns>0){
			for(int i = 1; i<Boards.size(); i++){
				System.out.println("Evaluating move: " + moves.get(i).toString());
				
				int j = evalPos(Boards.get(i), Integer.MIN_VALUE, Integer.MAX_VALUE, DEPTH, !sPlayer);
				if(j >= bestMoveScore){
					bestMove = moves.get(i);
					bestMoveScore = j;
				}
	
			}
		}else{
			Random z = new Random();
			int index = z.nextInt(moves.size());
			bestMove = moves.get(index);
		}
		System.out.println(bestMove.toString());
		numTurns++;
		return trigMove(b, bestMove); 

		
	}
	
	
	public String trigMove(Board b, Move moveMk){
		final String[] columns = {"A", "B", "C", "D", "E", "F", "G", "H"}; 
		Faction pcToMove = moveMk.getFaction();
		
		
		
		
		b.clrPos(moveMk.getOriginX(), moveMk.getOriginY());
		b.setPos(moveMk.getDestX(), moveMk.getDestY(), pcToMove);
		pcToMove.setLocation(moveMk.getDestX(), moveMk.getDestY());
		
		
		String text = pcToMove.getType() + " was moved to: " + columns[moveMk.getDestX()] +  (moveMk.getDestY()+1) + "\n";
		return text;
	}
	

	public int evalPos(Board b, int alpha, int beta, int depth, boolean kind){ 
		System.out.println("Processing at: depth-" + depth + "for- "+ kind);
		
		// Base case: if depth = 0, return the result of Heuristic
		
		if(depth == 0){
			int evaluation = Heuristic(b);
			System.out.println("Evaluated score: " + evaluation);
			return evaluation;
		}
		
		if(kind == false){ //min
			ArrayList<Move> moves = new ArrayList<Move>(); // possible moves from given pos
			
			 // get all poss moves of min
			 
			for(int i = 0; i<8; i++){
				for(int j=0; j<8; j++){
					if(b.hasaPiece(i,j)){
						if(b.getPos(i,j).getKind() == kind){
							Faction faction = b.getPos(i,j);
							for(int k =0; k<8; k++){
								for(int l=0; l<8; l++){
									Point p = new Point(k*62,l*62);
									if(faction.moveIsLegal(p, b)){
										moves.add(new Move(i,j,k,l,faction)); 
									}
									
								}
							}
						}
					}
				}
			}
			
			int newBeta = beta;
			for(Move move : moves){ //for child in node
				System.out.println("Moves that will be evaluated: " + move.toString());
				Board successorBoard = new Board(b); 
				trigMove(successorBoard, move);
				newBeta = Math.min(newBeta, evalPos(successorBoard, alpha, beta, depth -1, !kind)); 
				if(newBeta<= alpha) break;
			}
			return newBeta; //highest score of all poss moves
		}else { //max
			ArrayList<Move> moves = new ArrayList<Move>();
			
			for(int i = 0; i<8; i++){
				for(int j=0; j<8; j++){
					if(b.hasaPiece(i,j)){
						if(b.getPos(i,j).getKind() == kind){
							Faction faction = b.getPos(i,j);
							for(int k =0; k<8; k++){
								for(int l=0; l<8; l++){
									Point p = new Point(k*62,l*62);
									if(faction.moveIsLegal(p, b)){
										moves.add(new Move(i,j,k,l,faction));  
									}
									
								}
							}
						}
					}
				}
		}
		
		int newAlpha = alpha;
		for(Move move : moves){ //for child in node
			System.out.println("Moves that will be be evaluated: " + move.toString());
			Board successorBoard = new Board(b); 
			trigMove(successorBoard, move);
			newAlpha = Math.max(newAlpha, evalPos(successorBoard, alpha, beta, depth -1, !kind)); 
			if(beta<= newAlpha) break;
		}
		return newAlpha; //highest score of all posse moves
		}
	}
	
	
	public int Heuristic(Board b){
		int larvascore = 0;
		int birdscore = 0;

		/*
		 * Iterates through entire board.   
		 */
		for(int i = 0; i<8; i++){
			for(int j=0; j<8; j++){
				if(b.hasaPiece(i, j)){
					if(b.getPos(i, j).getKind() == false){ //case that faction is larva
						if(b.getPos(i,j).getType() == "Larva"){
							if(i <= 3) {larvascore += 10*(i + j + 1);}
							if(i > 3) {larvascore += 10*(j + (7 - i) + 1);}
							if(j == 7) {larvascore += 1000;} 
							try{ if(b.hasaPiece(i+1,j+1)) birdscore += 100;}
							catch(ArrayIndexOutOfBoundsException e){}
							try{ if(b.hasaPiece(i-1,j+1)) birdscore += 100;}
							catch(ArrayIndexOutOfBoundsException e){}	
							try{ if(b.hasaPiece(i+1,j-1)) birdscore += 100;}
							catch(ArrayIndexOutOfBoundsException e){}
							try{ if(b.hasaPiece(i-1,j-1)) birdscore += 100;}
							catch(ArrayIndexOutOfBoundsException e){}
												
						}
					}else if(b.getPos(i,j).getKind() == true){ //case that faction is bird
						if(b.getPos(i,j).getType() == "Bird"){
							if(i <= 3) {birdscore += 10*(i + j + 1);}
							if(i > 3) {birdscore += 10*(j + (7 - i) + 1);}
							
						}
					}
				}
			}
		}
		return (larvascore - birdscore); //returns larvascore-birdscore, Larva tries to maximize, Bird player tries to minimize
	}

}
